import router from '@/router'
import axios from 'axios'

const instance = axios.create({
  baseURL: 'http://localhost:3000'
})

function delay(sec: number) {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(sec), sec * 1000)
  })
}

instance.interceptors.request.use(
  async function (config) {
    const token = localStorage.getItem('access_token')
    console.log('request')
    if (token) {
      config.headers.Authorization = 'Bearer ' + token
    }
    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  async function (res) {
    console.log('response')
    await delay(1)
    return res
  },
  function (error) {
    if (401 === error.response.status) {
      console.log('requeasas  st')
      router.replace('/login')
    }
    return Promise.reject(error)
  }
)
export default instance
